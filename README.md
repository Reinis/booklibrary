# Book Library Web Application

MVC application that calls REST API to get library data and displays it in the web page.

```plantuml
@startuml
    skinparam handwritten false

    title Components of the solution

    boundary Commandline
    database Database
    control WebAPI #99ff99
    'entity Model
    'control Router
    boundary WebApp

    == Database ==
    Commandline -> Database: CSV import
    note right
        Import Goodreads
        library <&wrench>
    end note
    Commandline -> Database: Edit
    Commandline --> Database: Query
    Commandline <- Database: Query result

    hnote over Commandline
        C# TUI
    end note

    == WebAPI ==
    hnote over WebAPI
        SparkJava
    end note

    WebAPI -> Database
    WebAPI <- Database

    == Web app ==
    hnote over WebApp
        Vue.js
    end note

    WebAPI <-- WebApp: Get
    WebAPI <- WebApp: Post
    WebAPI -> WebApp: Response

@enduml
```

## Database

MariaDB — An Open Source fork of MySQL

There should be a possibility to manage the data outside the Web application
using a command line application written in C# (as an example).

### Data fields

```plantuml
@startuml
package "Library from Exercise70" {
    Class Author {
        - id
        - name
        - surname
        + getId()
        + setId(id)
        + getName()
        + setName(name)
        + getSurname()
        + setSurname(surname)
    }

    Class Book {
        - id
        - title
        - authorIds
        - yearReleased
        - coverImageUrl
        + getId()
        + setId(id)
        + getTitle()
        + setTitle(title)
        + getAuthorIds()
        + setAuthorIds(autorIds)
        + getYearReleased()
        + setYearReleased(yearReleased)
        + getCoverImageUrl()
        + setCoverImageUrl(coverImageUrl)
    }

    Class Library {
        - {static} libraryFile
        - authors
        - books
        + getLibraryFile()
        + getAuthors()
        + setAuthors(authors)
        + getBooks()
        + setBooks(books)
    }

    Class Main {
        - {static} {field} library
        --
        + main()
        - deserializeLibraryFromJsonFile()
        - serializeLibraryToJsonFile()
        .. response transformer ..
        - <T> serializeToJson(T element)
        ..
        - setRoutes()
        -- route handlers --
        .. /api/v1/authors ..
        - getAuthors()
        - addAuthor()
        - deleteAuthor()
        .. /api/v1/book/id ..
        - getBookById()
        .. /api/v1/books ..
        - getBooks()
        - addBook()
        - deleteBook()
        .. /api/v1/books/search ..
        - searchBooksByTitle()
        .. /api/v1/library ..
        - getLibrary()
        .. after filters ..
        - type("application/json")
        - header("Access-Control-Allow-Origin")
    }

    note right of Main
        Uses [[https://github.com/FasterXML/jackson Jackson]]
        for JSON (de)serialization.

        Uses [[http://sparkjava.com/ SparkJava]]
        for HTTP request handling.
    end note

    Author --o Library
    Book --o Library
    Library --o Main
}
@enduml
```

In Goodreads export there are many fields. Import most of them, but in the
model use only a subset.

Author:

-   Author
-   Author l-f

Book:

-   Book Id
-   Title
-   Author Ids
-   Additional Author Ids
-   ISBN
-   ISBN13
-   My Rating
-   Publisher Id
-   Binding
-   Number of Pages
-   Year Published
-   Original Publication Year
-   Date Read
-   Date Added
-   Bookshelves
-   Bookshelves with position
-   Exclusive shelf
-   My Review
-   Owned copies
-   Original Purchase Date

Publisher:

-   Publisher

### Data import

For the project I'll be using my Goodreads library. There is an option to
export it as CSV file. I can try to write a SQL importer for it.

There is also an API (though it requires developers to register and obtain an
api key) to access Goodreads data. Users can save their personal library
permanently, but other data can't be saved. It would require to implement OAuth
for user authentication.

There are several options for reading CSV files in C# [[1](https://dotnetcoretutorials.com/2018/08/04/csv-parsing-in-net-core/)]. For ease of use, I
should use CsvHelper [[2](https://joshclose.github.io/CsvHelper/)].

[1] https://dotnetcoretutorials.com/2018/08/04/csv-parsing-in-net-core/

[2] https://joshclose.github.io/CsvHelper/

### Cover images

For now, lets put them is static files. Later, I might put them in the database
in a separate table or use some other solution.

Should avoid repeated encoding/decoding of images while serving them to the Web
page.

## REST API

SparkJava — A simple Java framework (development stalled)

Play Framework has an internal JPA implementation, but it might complicate the
project quite a lot more. I want to keep the database data plain and
language-agnostic (not storing Java persistence objects).

## MVC app

Play Framework — Full-featured Web framework for Java and Scala

Model — Library class with Book and Author classes
View — Twirl templates generating HTML, CSS and JS
Controller — Server side of the Web app

## Web app

Vue.js — Progressive JavaScript framework

Views
Components
Routes

Note: Vue child objects are found under \$children, but DOM child objects are found under \$childNodes.

width — height
w     — h

width = w * height / h = height * aspect
height = h * width / w = width / aspect

## Static vs dynamically generated website

I might change my mind and discard the MVC part and use some JS framework on
client side (though they are not mutually exclusive). I still would need to run
a web server for frontend to work.

## Mobile app

Flutter — Cross-platform app development framework in Dart

Connect to the REST API and sync local database. Otherwise display and manage
the book database.

There is also an option to make a Wep app with Flutter, but priority should be
the mobile apps for Android and iOS.

Very little of the knowledge needed for this was covered in the course, so this
should be left to the end as an extra project.
